;;; packages.el --- polymode layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author:  <reitero@irene>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3
;;; Code:

(defconst polymode-packages
  '(
    polymode
    (poly-noweb :location elpa)
    (poly-markdown :location elpa)
    (poly-R :location elpa)
    )
  )

;; (defun polymode/init-polymode ()
;;   (use-package polymode
;;     :mode (("\\.Rmd"   . Rmd-mode))
;;     :init
;;     (progn
;;       (defun Rmd-mode ()
;;         "ESS Markdown mode for Rmd files"
;;         (interactive)
;;         (require 'poly-noweb)
;;         (require 'poly-R)
;;         (require 'poly-markdown)
;;         (R-mode)
;;         (poly-markdown+r-mode))
;;       ))
;;   )


(defun polymode/init-polymode ()
  (use-package polymode
    :mode (("\\.Rmd" . poly-markdown+r-mode))
    :init
    (autoload 'r-mode "ess-site.el" "Major mode for editing R source." t)
    :defer t
    )
  )

(defun polymode/init-poly-noweb ()
  (use-package poly-noweb
    :ensure t
    :defer t
    ;; :init
    ;; (require 'poly-noweb)
    )
  )

(defun polymode/init-poly-markdown ()
  (use-package poly-markdown
    :ensure t
    :defer t
    ;; :init
    ;; (require 'poly-markdown)
    )
  )

(defun polymode/init-poly-R ()
  (use-package poly-R
    ;; :after (poly-noweb poly-markdown)
    ;; :requires (poly-noweb)
    :ensure t
    :defer t
    ;; :init
    ;; (progn
    ;;   (use-package 'poly-noweb)
    ;;   ;; (require 'poly-R)
    ;;   )
    )
  )

;; packages.el ends here
