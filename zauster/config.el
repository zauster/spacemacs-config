;;------------------------------------------------------------
;;
;; General config
;;

;; enable PRIMARY selection for clipboard copying/pasting
(setq select-enable-primary t)

;; text filling: fill-column
(setq-default fill-column 65)


;;------------------------------------------------------------
;;
;; Programming stuff
;;

;; Disables Semantic mode for elisp-files as they often take a very long
;; timespan to be parsed

;; (semantic :disabled-for emacs-lisp)
(eval-after-load 'semantic
  (add-hook 'semantic-mode-hook
            (lambda ()
              (dolist (x (default-value 'completion-at-point-functions))
                (when (string-prefix-p "semantic-" (symbol-name x))
                  (remove-hook 'completion-at-point-functions x))))))

;; avy key selection: use Neo v2 home row
(setq avy-keys '(?u ?i ?a ?e ?o ?s ?n ?r ?t ?d))


;;------------------------------------------------------------
;;
;; Org-mode
;;

;; Set to the location of your Org files on your local system
(setq org-directory "~/Dropbox/Org")
;; Set to the files (or directory of files) you want sync'd
(setq org-agenda-files (quote ("~/Dropbox/Org")))

;; Set org-refile targets as all agenda files
(setq org-refile-use-outline-path 'file)
;; Do not complete in steps, does not work with helm
(setq org-outline-path-complete-in-steps nil)
;; Ask before refiling under newly created header
(setq org-refile-allow-creating-parent-nodes 'confirm)
;; First element for refiling is the current file, the second
;; element or other files. For both I use only the first two
;; levels of headlines for refiling
(setq org-refile-targets
      '((nil :maxlevel . 2)
        (org-agenda-files :maxlevel . 2)))
;; For future: if a lot of heading, caching may help
;; (setq org-refile-use-cache t)

;; Let the mini-calendar start with monday
(setq calendar-week-start-day 1)
;; Warn me of any deadlines in next 7 days
(setq org-deadline-warning-days 7)
;; Show me tasks scheduled or due in next fortnight
(setq org-agenda-span (quote fortnight))
;; Don't show tasks as scheduled if they are already shown as a
;; deadline
(setq org-agenda-skip-scheduled-if-deadline-is-shown t)
;; Don't give awarning colour to tasks with impending deadlines
;; if they are scheduled to be done
(setq org-agenda-skip-deadline-prewarning-if-scheduled
      (quote pre-scheduled))

;; Capture mode, default notes file. Move everything to inbox and
;; refile from there
(setq org-default-notes-file (concat org-directory "/inbox.org"))
(setq org-capture-templates
      (quote (
              ;;----------------------------------------
              ;; Most general inbox, collection of everything
              ("i" "Inbox" entry
               (file "~/Dropbox/Org/inbox.org")
               "* %? \n\nAdded: %U \nAt: %a"
               :empty-lines 1)

              ;;----------------------------------------
              ;; Org templates for work-related stuff
              ("w" "Work related")

              ;; File a meeting
              ("wm" "Meeting" entry
               (file "~/Dropbox/Org/work_meetings.org")
               "* SCHEDULED MEETING %? :MEETING:\nSCHEDULED: %^T"
               :empty-lines 1)

              ;; File a work ToDo, default: scheduled now
              ("wt" "ToDo" entry
               (file "~/Dropbox/Org/work_todo.org")
               "* TODO %? :WORK:\nSCHEDULED: %T\n\nAdded: %U \nAt: %a"
               :empty-lines 1)

              ;; File a work-related research idea
              ("wi" "Research idea" entry
               (file "~/Dropbox/Org/research_ideas.org")
               "* %? :IDEA:\n\nAdded: %U \nAt: %a"
               :empty-lines 1)

              ;;----------------------------------------
              ;; Personal ToDos
              ("p" "Personal ToDo" entry
               (file "~/Dropbox/Org/personal_todo.org")
               "* %? :PERSONAL: \n\nAdded: %U \nAt: %a"
               :empty-lines 1)
              )))


;;------------------------------------------------------------
;;
;; R + ESS configs
;;
(setq ess-ask-for-ess-directory nil)
(setq ess-ask-about-transfile nil)


;;------------------------------------------------------------
;; ;;
;; ;; Stata ado-mode
;; ;;
;; (when (string-equal system-type "windows-nt")
;;   (setq load-path (cons "~/.emacs.d/elpa/ado-mode-1.15.1.0/lisp" load-path))
;;   (require 'ado-mode)
;;   (setq ado-confirm-overwrite-flag t)
;;   ;; (setq ado-new-dir "W:/Reiter/Projects/2015_WIODv2/02_DoFiles")
;;   (setq ado-comeback-flag t)
;;   )


;;------------------------------------------------------------
;;
;; Helm Configuration
;;
(defun nm-around-helm-buffers-sort-transformer (candidates source)
  candidates)

(advice-add 'helm-buffers-sort-transformer
            :override #'nm-around-helm-buffers-sort-transformer)
