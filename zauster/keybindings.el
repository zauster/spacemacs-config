;; Switch h and l to be more coherent on my keyboard
(define-key evil-motion-state-map "h" 'evil-forward-char)
(define-key evil-motion-state-map "l" 'evil-backward-char)

(spacemacs/set-leader-keys (kbd "w h") 'evil-window-right)
(spacemacs/set-leader-keys (kbd "w l") 'evil-window-left)

;; quick cut/copy/paste keys
(define-key evil-motion-state-map "ü" 'kill-region)
(define-key evil-motion-state-map "ö" 'copy-region-as-kill)
(define-key evil-motion-state-map "ä" 'yank)
(global-set-key (kbd "C-ü") 'kill-region)         ;; Cut
(global-set-key (kbd "C-ö") 'copy-region-as-kill) ;; Copy
(global-set-key (kbd "C-ä") 'yank) ;; Paste

;; set the escape sequence to zb
(setq-default evil-escape-key-sequence "zb")
(setq evil-escape-unordered-key-sequence t)

;; Set C-d to scroll-down in inferior-ess-r-map
(eval-after-load "ess-r-mode"
  '(define-key inferior-ess-mode-map (kbd "C-d") 'evil-scroll-down)
  )


;; yas-expand
(define-key evil-insert-state-map (kbd "C-<tab>")
  'yas-expand)

;;
;; text: find and replace with anzu
(define-key evil-normal-state-map (kbd "SPC x q")
  'anzu-query-replace-regexp)

;;
;; Comment or uncomment the region/line
(global-set-key (kbd "M-ä") 'comment-line)

;; (define-key global-map [f1] 'org-agenda-list)
;; (define-key global-map [f2] 'org-todo-list)
;; (define-key global-map [f3] 'magit-status)
;; (define-key global-map [f4] 'dired)
(define-key global-map [f5] 'other-window)
(define-key global-map [f6] 'lazy-helm/helm-mini)
;; (define-key global-map [f7] 'kill-buffer)
;; (define-key global-map [f8] 'eval-region)
;; (define-key global-map [f9] 'kmacro-start-macro)
;; (define-key global-map [f10] 'kmacro-end-macro)
;; (define-key global-map [f11] 'call-last-kbd-macro)
;; (define-key global-map [f12] 'org-capture)
